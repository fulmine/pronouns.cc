# Privacy policy

By using pronouns.cc, you consent to having your data processed by the service.
If you do not agree to this, do not use the website.

This is the data pronouns.cc collects indefinitely:

- Any data you directly give the website (including names, pronouns, and avatars)
- Data pertaining to linked accounts (e.g. your Discord username and user ID, or your fediverse username and user ID)
- Data pertaining to reports made on users and members
- The minimum amount of session data needed to make the website work
- Daily database backups containing the above information

This is the data pronouns.cc stores in memory or in short-term persistent form (up to 30 days):

- High-level logs of API methods used on the website (not including request and response bodies)
- User data of accounts pending deletion, for recovery purposes

This is the data pronouns.cc does _not_ collect:

- Anything not listed above, including:
- Data of deleted members
- Data of users who deleted their account more than 30 days ago
- Information added _and deleted_ between daily backups

All user and member information, except when authentication-related, is publicly available, both through the website and the public API.

If you are logged in, you can export your data at any time [here](/settings).
If you want to delete your account, you can do so [here](/settings/export/).

Note that if you choose to delete the account, your data will be retained internally for thirty days to facilitate account recovery should you change your mind.
After thirty days, it is deleted permanently from our systems.
However, your data will immediately stop being accessible through the website.
You can also immediately delete your data by logging in again within this thirty day period.

If your account was suspended by a moderator after a report, you will not be able to export your data yourself.
Accounts that have been suspended by a moderator will be deleted 180 days after being suspended, or you can delete it yourself by attempting to log in during this 180 day period.
If you want your data exported, please contact me [here](/page/about).
